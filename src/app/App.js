import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";
import { useSelector } from "react-redux";
import TableContainer from "../table/table.container";
import ManagementContainer from "../management/management.container";
import LoginContainer from "../login/login.container";
import LogoutContainer from "../logout/logout.container";

function App() {
  const isAuthenticated = useSelector((state) => state.isAuthenticated);
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          {isAuthenticated ? <Redirect to="/management" /> : <LoginContainer />}
        </Route>
        <Route path="/management">
          <div className="management">
            <LogoutContainer />
            <div className="management-wrapper">
              <ManagementContainer />
              <TableContainer />
            </div>
          </div>
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
