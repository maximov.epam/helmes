import { LOGIN } from "../consts/actions-types";

export const login = (payload) => ({ type: LOGIN, payload });
