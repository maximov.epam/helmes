import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Form, Field } from "react-final-form";
import { LOGIN_FIELDS } from "../consts/fields";
import { ADMIN_CREDS } from "../consts/credentials";
import { useHistory } from "react-router-dom";
import { login } from "./login.action";
import isEqual from "lodash.isequal";
import "../management/management.css";

function LoginContainer() {
  const history = useHistory();
  const dispatch = useDispatch();
  const isAuthenticated = useSelector((state) => state.isAuthenticated);
  const users = useSelector((state) => state.users);

  const onSubmit = (creds) => {
    if (
      isEqual(ADMIN_CREDS, creds) ||
      users.find((user) => user["First name"] === creds.name)
    ) {
      dispatch(login());
    }
  };

  useEffect(() => {
    isAuthenticated && history.push("/management");
  });

  return (
    <div className="container-lg users-wrapper">
      <Form
        onSubmit={onSubmit}
        render={({ handleSubmit }) => (
          <form onSubmit={handleSubmit}>
            <h1 className="heading">Sign in</h1>
            <div className="fields">
              {LOGIN_FIELDS.map((loginField, i) => (
                <div className="mb-3 me-3 p-3 wrapper" key={i}>
                  <label htmlFor={LOGIN_FIELDS} className="form-label label">
                    {loginField}
                  </label>
                  <Field name={loginField}>
                    {({ input }) => (
                      <input
                        type={loginField === "password" ? "password" : "text"}
                        {...input}
                        className="form-control"
                      />
                    )}
                  </Field>
                </div>
              ))}
            </div>
            <button onClick={handleSubmit} className="btn submit-button">
              SUBMIT
            </button>
          </form>
        )}
      />
    </div>
  );
}

export default LoginContainer;
