import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import LogoutContainer from "../logout/logout.container";
import { useDispatch } from "react-redux";
import { keepAlive } from "./modal.action";

function rand() {
  return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
  const top = 50 + rand();
  const left = 50 + rand();

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: "absolute",
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

export default function KeepAliveModal(props) {
  const { open, setOpen } = props;
  const dispatch = useDispatch();
  const handleKeepAlive = () => {
    dispatch(keepAlive());
    setOpen(false);
  };
  const classes = useStyles();
  const [modalStyle] = React.useState(getModalStyle);

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <Modal
      open={open}
      onClose={handleClose}
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
    >
      <div style={modalStyle} className={classes.paper}>
        <h2 id="simple-modal-title">Session expires soon</h2>
        <p id="simple-modal-description">Do you want to continue?</p>
        <LogoutContainer />
        <button
          className="btn submit-button mb-3 me-3"
          onClick={handleKeepAlive}
        >
          Continue
        </button>
      </div>
    </Modal>
  );
}
