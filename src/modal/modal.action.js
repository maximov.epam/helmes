import { KEEP_ALIVE } from "../consts/actions-types";

export const keepAlive = () => ({ type: KEEP_ALIVE });
