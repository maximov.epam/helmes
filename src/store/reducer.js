import {
  ADD_USER,
  DELETE_USER,
  KEEP_ALIVE,
  LOGIN,
  LOGOUT,
} from "../consts/actions-types";

function reducer(state, action) {
  switch (action.type) {
    case ADD_USER:
      return {
        ...state,
        users: [...state.users, action.payload],
      };
    case DELETE_USER:
      return {
        ...state,
        users: [
          ...state.users.slice(0, action.payload),
          ...state.users.slice(++action.payload),
        ],
      };
    case LOGIN: {
      return {
        ...state,
        isAuthenticated: true,
        users: state.users || [], // needs as a default state, cause redux-persist don't have it
      };
    }
    case LOGOUT: {
      return {
        ...state,
        isAuthenticated: false,
      };
    }
    case KEEP_ALIVE: {
      return {
        ...state,
      };
    }
    default:
      return state;
  }
}

export default reducer;
