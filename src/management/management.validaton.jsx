import React from "react";
import { MANAGEMENT_FIELDS } from "../consts/fields";
import { VALIDATION_MAP } from "../consts/regex";

export const matchRegex = (e) => {
  const errors = {};
  MANAGEMENT_FIELDS.forEach((field) => {
    if (e[field] && !e[field].match(VALIDATION_MAP[field])) {
      errors[field] = "Not valid";
    }
    if (e["Password"] !== e["Repeat password"]) {
      errors[["Repeat password"]] = "Not the same";
    }
    if (e["Password"] && e["Password"].length < 5) {
      errors[["Password"]] = "Too short";
    }
  });
  return errors;
};

export const exceedMaxLength = (e) => {
  const MAX_LENGTH = 30;
  if (e.length > MAX_LENGTH) {
    return "Max length exceed";
  }
};

export const requireField = (e) => {
  if (!e || e === "") {
    return "This field is required";
  }
};
