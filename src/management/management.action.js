import { ADD_USER, DELETE_USER } from "../consts/actions-types";

export const addUser = (payload) => ({ type: ADD_USER, payload });
export const deleteUser = (payload) => ({ type: DELETE_USER, payload });
