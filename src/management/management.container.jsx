import React, { useEffect, useState } from "react";
import { Form } from "react-final-form";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";
import AddUser from "./add-user/add-user";
import { addUser } from "./management.action";
import { matchRegex } from "./management.validaton";
import { logout } from "../logout/logout.action";
import KeepAliveModal from "../modal/modal";
import "./management.css";

function ManagementContainer() {
  const dispatch = useDispatch();
  const store = useSelector((state) => state);
  const [currentTime, setCurrentTime] = useState(moment.utc());
  const [timeToShowModal, setTimeToShowModal] = useState(moment.utc());
  const [timeToLogout, setTimeToLogout] = useState(moment.utc());
  const [open, setOpen] = useState(false);
  const CHECKING_INTERVAL = 1000;

  const onSubmit = (user) => {
    dispatch(addUser(user));
  };

  const checkExceedTime = () => {
    if (timeToShowModal.isBefore(currentTime)) {
      setOpen(true);
    }
    if (timeToLogout.isBefore(currentTime)) {
      dispatch(logout());
    }
  };

  useEffect(() => {
    setTimeToShowModal(moment.utc().add(2, "minutes"));
    setTimeToLogout(moment.utc().add(3, "minutes"));
  }, [store]);

  useEffect(() => {
    const checkingInterval = setInterval(() => {
        setCurrentTime(moment.utc());
      }, CHECKING_INTERVAL);
    return clearInterval(checkingInterval);
  }, []);

  useEffect(() => {
    checkExceedTime();
  }, [currentTime]);

  return (
    <div className="container-lg users-wrapper">
      <KeepAliveModal open={open} setOpen={setOpen} />
      <h1 className="heading">Add user</h1>
      <Form
        onSubmit={onSubmit}
        validate={matchRegex}
        render={({ handleSubmit }) => (
          <form onSubmit={handleSubmit}>
            <div className="fields">
              <AddUser />
            </div>
            <button onClick={handleSubmit} className="btn submit-button">
              Add
            </button>
          </form>
        )}
      />
    </div>
  );
}

export default ManagementContainer;
