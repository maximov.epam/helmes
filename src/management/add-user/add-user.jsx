import React from "react";
import { Field } from "react-final-form";
import { MANAGEMENT_FIELDS } from "../../consts/fields";
import { exceedMaxLength, requireField } from "../management.validaton";
import "./add-user.css";

const AddUser = () => {
  const composeValidators =
    (...validators) =>
    (value) => {
      return validators.reduce(
        (error, validator) => error || validator(value),
        null
      );
    };

  return MANAGEMENT_FIELDS.map((field, i) => (
    <div className="mb-3 me-3 p-3 wrapper" key={i}>
      <label htmlFor={field} className="form-label label">
        {field}
      </label>
      <Field
        name={field}
        validate={composeValidators(requireField, exceedMaxLength)}
      >
        {({ input, meta }) => (
          <div>
            <input
              type={
                field.toLowerCase().includes("password") ? "password" : "text"
              }
              {...input}
              className="form-control"
            />
            {(meta.touched && meta.error && (
              <span className="validation-error">{meta.error}</span>
            )) || <span className="hidden-elem">Content</span>}
          </div>
        )}
      </Field>
    </div>
  ));
};

export default AddUser;
