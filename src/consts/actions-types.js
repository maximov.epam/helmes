export const ADD_USER = "ADD_USER";
export const DELETE_USER = "DELETE_USER";
export const LOGIN = "LOGIN";
export const LOGOUT = "LOGOUT";
export const KEEP_ALIVE = "KEEP_ALIVE";
