export const LOGIN_FIELDS = ["name", "password"];

export const MANAGEMENT_FIELDS = [
  "First name",
  "Last name",
  "Email",
  "Username",
  "Password",
  "Repeat password",
];
