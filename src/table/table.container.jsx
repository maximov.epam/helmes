import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { MANAGEMENT_FIELDS } from "../consts/fields";
import DeleteOutlineIcon from "@material-ui/icons/DeleteOutline";
import { deleteUser } from "../management/management.action";
import "./table.css";

function TableContainer() {
  const dispatch = useDispatch();
  const users = useSelector((state) => state.users);
  const deleteOption = (userID) => (
    <td key="key" onClick={() => dispatch(deleteUser(userID))}>
      {<DeleteOutlineIcon />}
    </td>
  );
  const tableVisibleFields = [...MANAGEMENT_FIELDS.slice(0, 4)];

  const showTableContent = () =>
    users?.map((user, index) => {
      return (
        <tr key={index}>
          {[
            tableVisibleFields.map((field, index) => (
              <td key={index}>{user[field]}</td>
            )),
            deleteOption(index),
          ]}
        </tr>
      );
    });

  return (
    <table className="mx-3 mb-0 table">
      <thead>
        <tr>
          {[...tableVisibleFields, "Actions"].map((field, index) => (
            <th key={index} scope="col">
              {field}
            </th>
          ))}
        </tr>
      </thead>
      <tbody>{showTableContent()}</tbody>
    </table>
  );
}

export default TableContainer;
