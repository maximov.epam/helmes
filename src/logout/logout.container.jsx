import React, { useEffect } from "react";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { logout } from "./logout.action";

const LogoutContainer = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const isAuthenticated = useSelector((state) => state.isAuthenticated);

  const handleLogout = () => {
    dispatch(logout());
    history.push(`/`);
  };

  useEffect(() => {
    !isAuthenticated && handleLogout();
  });

  return (
    <button className="btn submit-button mb-3 me-3" onClick={handleLogout}>
      Logout
    </button>
  );
};

export default LogoutContainer;
