import { LOGOUT } from "../consts/actions-types";

export const logout = () => ({ type: LOGOUT });
